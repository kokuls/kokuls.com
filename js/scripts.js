$(document).ready(function() {
    $('.dynamo').dynamo();
    var mainbottom = $('#headertext').offset().top + $('#headertext').height();
    $(window).on('scroll', function() {
        stop = Math.round($(window).scrollTop());
        if (stop > mainbottom) {
            $('nav').addClass('past-main');
        } else {
            $('nav').removeClass('past-main');
        }
    });
});

function loadProject(className) {
    var loadFrom = "projects.html" + " " + className;
    $(".proj-prev").load(loadFrom, function() {
        $(".proj-prev").show();
        $('html,body').animate({
            scrollTop: $(".proj-prev").offset().top - 50
        }, 400);
        $(".close").click(function() {
            $(".proj-prev").hide();
            $('html,body').animate({
                scrollTop: $("#projects").offset().top -
                    50
            }, 500);
        })
    });
}

$("#mcgill").click(function() {
    loadProject(".mcgill-desc");
});
$("#acc-g1").click(function() {
    loadProject(".acc-g1-desc");
});
$("#sharestreet").click(function() {
    loadProject(".sharest-desc");
});
$("#portfolio").click(function() {
    loadProject(".portf-desc");
});

$(function() {
    $('a[href*=#]').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname
            .replace(/^\//, '') && location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this
                .hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 70
                }, 500);
                return false;
            }
        }
    });
});
